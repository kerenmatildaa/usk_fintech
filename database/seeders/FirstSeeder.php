<?php

namespace Database\Seeders;


use App\Models\Barang;
use App\Models\Role;
use App\Models\Saldo;
use App\Models\Transaksi;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class FirstSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin     = Role::create(["name" => "Administrator"]);
        $bank_mini = Role::create(["name" => "Bank Mini"]);
        $kantin    = Role::create(["name" => "Kantin"]);
        $siswa     = Role::create(["name" => "Siswa"]);

        User::create([
            "name" => "ira",
            "email" => "ira@gmail.com",
            "password" => Hash::make("ira"),
            "role_id" => $admin->id
        ]);

        User::create([
            "name" => "doya",
            "email" => "doya@gmail.com",
            "password" => Hash::make("doya"),
            "role_id" => $bank_mini->id 
        ]);

        User::create([
            "name" => "sipa",
            "email" => "sipa@gmail.com",
            "password" => Hash::make("sipa"),
            "role_id" => $kantin->id
        ]);

        $keren = User::create([
            "name" => "keren",
            "email" => "keren@gmail.com",
            "password" => Hash::make("keren"),
            "role_id" => $siswa->id
        ]);

        Barang::create([
            "name" => "gesper",
            "image" => "gesper.jpg",
            "price" => 5000,
            "stock" => 10,
            "desc" => "gesper smk"
        ]);

        Saldo::create([
            "user_id" => $keren->id,
            "saldo" => 50000

        ]);

        Transaksi::create([
            "user_id" => $keren->id,
            "barang_id" => null,
            "amount" => 3000,
            "invoice_id" => null,
            "type" => 1,
            "status" => 3
        ]);

    }
}
