<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use Illuminate\Http\Request;

class BankController extends Controller
{
    public function get_transaksi() {
        $transaksis = Transaksi::all();
        return view('pages.bank.index', compact('transaksis'));
    }
    public function acc_transaksi() {
        
    }
}